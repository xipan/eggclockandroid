﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EggClock.Models
{
    public class CookEggs
    {
        public enum CookEggType
        {
            SoftBoiled = 0,
            SmilingBoiled = 1,
            HardBoiled = 2
        }

        public enum CookEggStatus
        {
            Standby = 0, 
            Running = 1,
            End = 2
        }

        private const int SoftBoiledCookTime = 1;
        private const string SoftBoiledCookImage = "soft.png";
        private const int SmilingBoiledCookTime = 2;
        private const string SmilingBoiledCookImage = "medium.png";
        private const int HardBoiledCookTime = 3;
        private const string HardBoiledCookImage = "hard.png";


        /// <summary>
        /// 0: Soft Boiled, 1 : Smiling Boiled, 2: Hard Boiled
        /// </summary>
        public CookEggType CookType { get; set; }

        /// <summary>
        /// 0: standby, 1: running, 2: end
        /// </summary>
        public CookEggStatus CookStatus { get; set; }

        public TimeSpan CookTime { get; set; }

        public String CookImage { get; set; }

        public CookEggs(CookEggType cookType)
        {
            switch (cookType)
            {
                case CookEggType.SoftBoiled :
                    {
                        CookType = CookEggType.SoftBoiled;
                        CookStatus = CookEggStatus.Standby;
                        CookTime = new TimeSpan(0, 0, SoftBoiledCookTime, 0);
                        CookImage = SoftBoiledCookImage;
                        break;
                    }
                case CookEggType.SmilingBoiled:
                    {
                        CookType = CookEggType.SmilingBoiled;
                        CookStatus = CookEggStatus.Standby;
                        CookTime = new TimeSpan(0, 0, SmilingBoiledCookTime, 0);
                        CookImage = SmilingBoiledCookImage;
                        break;
                    }
                case CookEggType.HardBoiled:
                    {
                        CookType = CookEggType.HardBoiled;
                        CookStatus = CookEggStatus.Standby;
                        CookTime = new TimeSpan(0, 0, HardBoiledCookTime, 0);
                        CookImage = HardBoiledCookImage;
                        break;
                    }
                default:
                    break;
            }

        }

    }
}
