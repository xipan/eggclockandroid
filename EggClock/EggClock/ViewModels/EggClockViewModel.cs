﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using EggClock.Models;


namespace EggClock.ViewModels
{
    public class EggClockViewModel : BaseViewModel
    {
        private CookEggs currentCookingEgg;

        string softBoiledEggButtonLabel = string.Empty;
        public string SoftBoiledEggButtonLabel 
        {
            get { return softBoiledEggButtonLabel; }
            set { SetProperty(ref softBoiledEggButtonLabel, value); }
        }
        public bool IsSoftBoiledEggButtonEnable 
        {
            get
            {
                return IsEnableSelectButtonOfCookeType();
            }

        }

        string smilingBoiledEggButtonLabel = string.Empty;
        public string SmilingBoiledEggButtonLabel 
        {
            get { return smilingBoiledEggButtonLabel; }
            set { SetProperty(ref smilingBoiledEggButtonLabel, value); }
        }
        public bool IsSmilingBoiledEggButton
        {
            get
            {
                return IsEnableSelectButtonOfCookeType();
            }
        }

        string hardBoiledEggButtonLabel = string.Empty;
        public string HardBoiledEggButtonLabel 
        { 
            get { return hardBoiledEggButtonLabel; }
            set { SetProperty(ref hardBoiledEggButtonLabel, value); }
        }
        public bool IsHardBoiledEggButton
        {
            get
            {
                return IsEnableSelectButtonOfCookeType();
            }
        }

        public string CookeTimeLabel 
        {
            get
            {
                if ( this.currentCookingEgg == null )
                {
                    return "00:00";
                }

                return string.Format(
                    "{0:00}:{1:00}", 
                    currentCookingEgg.CookTime.Minutes, 
                    currentCookingEgg.CookTime.Seconds
                    );
            }
        }

        public string CookImage 
        { 
            get
            {
                if (this.currentCookingEgg == null)
                {
                    return string.Empty;
                }

                return currentCookingEgg.CookImage;
            }
        }

        string launcheButtonLabel = string.Empty;
        public string LauncheButtonLabel
        {
            get { return launcheButtonLabel; }
            set { SetProperty(ref launcheButtonLabel, value); }
        }
        public bool IsLauncheButton
        {
            get
            {
                return IsEnableLauncheButton();
            }
        }


        public Command SelectEggCommand { get; set; }

        public Command LauncheCook { get; set; }

        public EggClockViewModel()
        {
            Title = "Vælg æg";
            SoftBoiledEggButtonLabel = "Blødkogt";
            SmilingBoiledEggButtonLabel = "Smilende";
            HardBoiledEggButtonLabel = "Hårdkogt";
            LauncheButtonLabel = "Start";

            SelectEggCommand = new Command<string>( (eggType) => ExecuteSelectEggCommand(eggType));

            LauncheCook = new Command(
                () => ExecuteLauncheCookCommad(), 
                ()=> 
                    {
                        return IsEnableLauncheButton();
                    }
            );

        }

        private void ExecuteLauncheCookCommad()
        {
            switch (currentCookingEgg.CookStatus)
            {
                case CookEggs.CookEggStatus.Standby:
                    {
                        currentCookingEgg.CookStatus = CookEggs.CookEggStatus.Running;
                        LauncheButtonLabel = "Stop";
                        Cooking();
                        break;
                    }                    
                case CookEggs.CookEggStatus.Running:
                    {
                        currentCookingEgg.CookStatus = CookEggs.CookEggStatus.Standby;
                        LauncheButtonLabel = "Start";
                        break;
                    }                    
                default:
                    break;
            }
        }

        void ExecuteSelectEggCommand(string eggType)
        {
            //SoftBoiled = 0,
            //SmilingBoiled = 1,
            //HardBoiled = 2
            switch (eggType)
            {
                case "0":
                    {
                        this.currentCookingEgg = new CookEggs(CookEggs.CookEggType.SoftBoiled);
                        break;
                    }
                case "1":
                    {
                        this.currentCookingEgg = new CookEggs(CookEggs.CookEggType.SmilingBoiled);
                        break;
                    }
                case "2":
                    {
                        this.currentCookingEgg = new CookEggs(CookEggs.CookEggType.HardBoiled);
                        break;
                    }
                default:
                    this.currentCookingEgg = new CookEggs(CookEggs.CookEggType.SoftBoiled);
                    break;
            }
            RefreshProperty();
        }

        private void Cooking()
        {
            if ( currentCookingEgg.CookStatus == CookEggs.CookEggStatus.Running )
            {
                TimeSpan stepperCooking = new TimeSpan(0, 0, 0, 1);

                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    // Do something
                    currentCookingEgg.CookTime = currentCookingEgg.CookTime.Subtract(stepperCooking);
                    OnPropertyChanged("CookeTimeLabel");
                    if (
                        ((currentCookingEgg.CookTime.Seconds == 0) && (currentCookingEgg.CookTime.Minutes == 0)) ||
                        (currentCookingEgg.CookStatus == CookEggs.CookEggStatus.Standby)
                    )
                    {
                        if (
                            ((currentCookingEgg.CookTime.Seconds == 0) && (currentCookingEgg.CookTime.Minutes == 0)) && 
                            (currentCookingEgg.CookStatus == CookEggs.CookEggStatus.Running)
                        )
                        {
                            PlayAlertSound();
                        }

                        currentCookingEgg = null;
                        LauncheButtonLabel = "Start";
                        RefreshProperty();
                        return false; // True = Repeat again, False = Stop the timer                    
                    }
                    else
                    {
                        return true; // True = Repeat again, False = Stop the timer
                    }

                });
            }
        }

        private void RefreshProperty()
        {
            OnPropertyChanged("CookeTimeLabel");
            OnPropertyChanged("IsSoftBoiledEggButtonEnable");
            OnPropertyChanged("IsSmilingBoiledEggButton");
            OnPropertyChanged("IsHardBoiledEggButton");
            OnPropertyChanged("IsLauncheButton");
            OnPropertyChanged("CookImage");
        }
        private bool IsEnableSelectButtonOfCookeType()
        {
            if (this.currentCookingEgg == null)
            {
                return true;
            }

            if (this.currentCookingEgg.CookStatus == CookEggs.CookEggStatus.Running)
            {
                return false;
            }

            return true;
        }
        private bool IsEnableLauncheButton()
        {
            if (this.currentCookingEgg == null)
            {
                return false;
            }
            return true;
        }
        async void PlayAlertSound()
        {
            var player = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
            player.Load("ringsound.mp3");
            Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current.Loop = true;
            Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current.Play();
            
            await Application.Current.MainPage.DisplayAlert("Slut ", "Timer er udløbet", "OK");

            Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current.Stop();
        }
    }
}
