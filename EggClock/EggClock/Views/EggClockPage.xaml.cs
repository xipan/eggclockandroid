﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using EggClock.ViewModels;


namespace EggClock.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EggClockPage : ContentPage
    {
        EggClockViewModel viewModel;
        
        public EggClockPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new EggClockViewModel();
        }

    }
}